from pyraf import iraf
import os

band="R"
biasFeature="*bias*.fit"
flatFeature="*flat_"+band+"*.fit"
imageFeature="V463_"+band+"_*.fit"
flagBIAS = 1  #0 means that Zero.fits already exists.
flagFLAT = 1  #1 means that Flat.fits already exists.
filepath="/Users/xiaojia/xinglong/v463/20151212/"

# generate bias.lst
if flagBIAS == 1 :
    biasFileName=filepath+"Zeros.fits"
    os.system("ls "+filepath+biasFeature+"> "+filepath+"bias.lst")
    # zero combine 
    iraf.zerocombine("@"+filepath+"bias.lst",output=biasFileName)
    print "zerocombine success"

if flagFLAT == 1 :
    # genreate flat.lst
    os.system("ls "+filepath+flatFeature+"> "+filepath+"flat"+band+".lst") 
    #flat-bias
    iraf.imarith("@"+filepath+"flat"+band+".lst","-",biasFileName,"@"+filepath+"flat"+band+".lst//b");
    print "flat-bias success"
    #flat combine
    flatFileName=filepath+"flat"+band+".fits" # flatV.fits
    iraf.flatcombine("@"+filepath+"flat"+band+".lst//b",output=flatFileName)
    print "flat combine success"

#generate all image.lst
os.system("ls "+filepath+imageFeature+"> "+filepath+band+"image.lst")

#(image-bias)/flat
iraf.ccdproc("@"+filepath+band+"image.lst",zerocor="yes",flatcor="yes",zero=biasFileName,flat=flatFileName)
print "(image-bias)/flat success"
