from pyraf import iraf
import os
# generate bias.lst
filepath="/home/vancky/workshop_docs_softs/photometric_20130321_80_sx/"
band="R"
FWHM=6.0
STDDEV=3.0
fliename=filepath+"YZ_Boo_R_001.fit"
#set datapars
iraf.datapars.setParam("fwhmpsf",FWHM)
iraf.datapars.setParam("sigma",STDDEV)
iraf.datapars.setParam("exposur","EXPTIME")
#iraf.datapars(fwhmpsf=FWHM,sigma=STDDEV,exposur="EXPTIME")
print "datapars success"

#set findpars
iraf.findpars.setParam("threshold",4*STDDEV)
iraf.findpars.setParam("nsigma",min(2,0.426*STDDEV*FWHM))
#iraf.findpars(threshold=STDDEV,nsigma=min(2,0.426*STDDEV*FWHM))
print "findpars success"



#generate all image.lst
imageFeature="YZ_Boo_R_*.fit"
os.system("ls "+filepath+imageFeature+"> "+filepath+band+"image.lst")
#dao find
iraf.daophot.setParam("verify",No)
iraf.daofind("@"+filepath+band+"image.lst","@"+filepath+band+"image.lst//.coo.1",threshold=4*STDDEV,\
	nsigma=min(2,0.426*STDDEV*FWHM),fwhmpsf=FWHM,sigma=STDDEV,exposur="EXPTIME")
#iraf.centerpars()
print "daofind success"