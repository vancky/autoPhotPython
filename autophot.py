from pyraf import iraf
import os
# generate bias.lst
filepath="/home/vancky/workshop_docs_softs/photometric_20130321_80_sx/"
band="R"
FWHM=6.0
STDDEV=3.0
#set datapars
iraf.datapars.setParam("fwhmpsf",FWHM)
iraf.datapars.setParam("sigma",STDDEV)
iraf.datapars.setParam("exposur","EXPTIME")
#iraf.datapars(fwhmpsf=FWHM,sigma=STDDEV,exposur="EXPTIME")
print "datapars success"

iraf.centerpars.setParam("cbox",2*FWHM)
print 'centerpars success'

iraf.fitskypars.setParam("annulus",5*FWHM)
iraf.fitskypars.setParam("dannulus",2*FWHM)

iraf.photpars.setParam("apertures",1.5*FWHM)


#generate all image.lst
#iraf.daophot.setParam("verify",'NO')
print filepath+band+'Coo.lst'
iraf.phot("@"+filepath+band+"image.lst",coords="@"+filepath+band+'Coo.lst',output="@"+filepath+band+"image.lst//.mag")
#iraf.centerpars()
print "autophot success"
