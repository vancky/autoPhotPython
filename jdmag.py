from pyraf import iraf
import os
import numpy as np
band="R"
filepath="/home/vancky/workshop_docs_softs/photometric_20130321_80_sx/"
iraf.txdump(filepath+"*"+band+'*.mag.fit','mag','yes',Stdout=filepath+band+'mag.txt')

iraf.setjd("@"+filepath+band+"image.lst",observatory='bao',date='date-obs',time='time-obs',exposure='exptime',ra='ra',dec='dec',hjd='',ljd='',Stdout=filepath+band+'jd.txt')
print 'jdmag success'
